﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class MsStore
    {
        public long PkStoreId { get; set; }
        public string StoreName { get; set; }
        public string StoreDescription { get; set; }
        public string StoreMerchantName { get; set; }
        public bool? StoreStatus { get; set; }
        public long? FkLoginId { get; set; }
    }
}
