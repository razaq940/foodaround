﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class MsLogin
    {
        public long PkLoginId { get; set; }
        public string LogingPhoneNumber { get; set; }
        public string LoginPin { get; set; }
        public bool? LoginActivation { get; set; }
        public string LoginSalt { get; set; }
    }
}
