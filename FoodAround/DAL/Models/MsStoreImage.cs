﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class MsStoreImage
    {
        public long PkStoreImageId { get; set; }
        public string StoreImagePath { get; set; }
        public string StoreImageName { get; set; }
        public string StoreImageGuid { get; set; }
        public long? FkStoreId { get; set; }
    }
}
