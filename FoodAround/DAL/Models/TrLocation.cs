﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class TrLocation
    {
        public long PkLocationId { get; set; }
        public string LocationLongtitude { get; set; }
        public string LocationLatitude { get; set; }
        public DateTime? LocationDate { get; set; }
        public long? FkStoreId { get; set; }
    }
}
