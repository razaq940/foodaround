﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class TrOtp
    {
        public long PkOtpId { get; set; }
        public string Otp { get; set; }
        public long? FkLoginId { get; set; }
    }
}
