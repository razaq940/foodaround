﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace DAL.Models
{
    public partial class FoodAroundContext : DbContext
    {
        public FoodAroundContext()
        {
        }

        public FoodAroundContext(DbContextOptions<FoodAroundContext> options)
            : base(options)
        {
        }

        public virtual DbSet<MsLogin> MsLogins { get; set; }
        public virtual DbSet<MsStore> MsStores { get; set; }
        public virtual DbSet<MsStoreImage> MsStoreImages { get; set; }
        public virtual DbSet<TrLocation> TrLocations { get; set; }
        public virtual DbSet<TrOtp> TrOtps { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Password=123Data;Persist Security Info=True;user id=abraz;Initial Catalog=FoodAround;Data Source=localhost;TrustServerCertificate=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<MsLogin>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ms_login");

                entity.Property(e => e.LoginActivation).HasColumnName("login_activation");

                entity.Property(e => e.LoginPin)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("login_pin");

                entity.Property(e => e.LoginSalt)
                    .IsUnicode(false)
                    .HasColumnName("login_salt");

                entity.Property(e => e.LogingPhoneNumber)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("loging_phone_number");

                entity.Property(e => e.PkLoginId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("pk_login_id");
            });

            modelBuilder.Entity<MsStore>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ms_store");

                entity.Property(e => e.FkLoginId).HasColumnName("fk_login_id");

                entity.Property(e => e.PkStoreId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("pk_store_id");

                entity.Property(e => e.StoreDescription)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("store_description");

                entity.Property(e => e.StoreMerchantName)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("store_merchant_name");

                entity.Property(e => e.StoreName)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("store_name");

                entity.Property(e => e.StoreStatus).HasColumnName("store_status");
            });

            modelBuilder.Entity<MsStoreImage>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ms_store_image");

                entity.Property(e => e.FkStoreId).HasColumnName("fk_store_id");

                entity.Property(e => e.PkStoreImageId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("pk_store_image_id");

                entity.Property(e => e.StoreImageGuid)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("store_image_guid");

                entity.Property(e => e.StoreImageName)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("store_image_name");

                entity.Property(e => e.StoreImagePath)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("store_image_path");
            });

            modelBuilder.Entity<TrLocation>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tr_location");

                entity.Property(e => e.FkStoreId).HasColumnName("fk_store_id");

                entity.Property(e => e.LocationDate)
                    .HasColumnType("datetime")
                    .HasColumnName("location_date");

                entity.Property(e => e.LocationLatitude)
                    .IsUnicode(false)
                    .HasColumnName("location_latitude");

                entity.Property(e => e.LocationLongtitude)
                    .IsUnicode(false)
                    .HasColumnName("location_longtitude");

                entity.Property(e => e.PkLocationId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("pk_location_id");
            });

            modelBuilder.Entity<TrOtp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tr_otp");

                entity.Property(e => e.FkLoginId).HasColumnName("fk_login_id");

                entity.Property(e => e.Otp)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("otp");

                entity.Property(e => e.PkOtpId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("pk_otp_id");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
